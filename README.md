# UCSC ECE163 UAV Modeling and Control GUI

This is the UCSC ECE163 UAV Modeling and Control GUI.  At the moment, it is incomplete, as many key modules have been replaced with stubs.   As you go through the class, you'll replace these modules with your own code.

This is your top-level README.  As we go through the course, you'll use this README to convey information about the state of your project to your graders, but for now, just replace this line with your name.

## Chapter2.py
 Launches a version of the GUI that applies rotations and translations to an aircraft model.  Currently non-functional.